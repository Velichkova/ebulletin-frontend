import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { Subscription } from 'rxjs';
import { User, UserRole } from '../types/user.type';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.css']
})
export class ViewUsersComponent implements OnInit {

  users: User[];
  user: User;
  editor: boolean;

  constructor(private userService:UsersService, 
    private authService:AuthService) { }
  private subscription: Subscription = new Subscription();
  
  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers(){
    this.subscription.add(this.userService.getUsers().subscribe(data => {
      this.users = data;
    }));
  }

  changeRoleButton(user: User){
    this.user = this.authService.getUser();
    if(user.role.id == 1){
      user.role.id = 3;
      this.editor = true
      this.subscription.add(this.userService.updateUser(user).subscribe(data => {
      }));
    }else{
      user.role.id = 1;
      this.editor = false;
      this.subscription.add(this.userService.updateUser(user).subscribe(data => {
      }));
    }
    window.location.reload();
  }
}
