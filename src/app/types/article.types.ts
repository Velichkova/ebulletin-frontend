import { User } from './user.type';

export interface Article {
    id: string;
    articleFile?: string;
    user: User;
    bulletin?: Bulletin;
    title: string;
	  authors: string;
    abstractText: string;
    createdDate: Date;
    lastModifiedDate: Date;
    feeImage?: string;
  }
  
  export interface Bulletin {
    id: string;
    startDate: Date;
    endDate: Date;
    bulletinFile?: string;
    issn: string;
    vol: string;
    editionYear: string;
  }

  export interface Review {
    id: string;
    reviewFile?: string;
    accepted?: boolean;
    article: Article;
    user: User;
    createdDate: Date;
  }