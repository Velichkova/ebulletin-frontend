export interface User {
    id: string;
    username: string;
    password?: string;
    name: string;
    email: string;
    phone: string;
    faculty: string;
    role: UserRole;
    forgottenPasswordToken: string;
    createdDate: Date;
    lastModifiedDate: Date;
  }
  
  export interface UserRole {
    id: number;
    name: string;
    description: string;
  }