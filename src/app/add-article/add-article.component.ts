import { Component, OnInit } from '@angular/core';
import { Article } from '../types/article.types';
import { Subscription } from 'rxjs';
import { ArticleService } from '../services/article.service';
import { User } from '../types/user.type';
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { InternacionalisationService } from '../shared/i18n.service';
import { addArticleMessages } from '../shared/i18n.messages';


@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {

  article: Article;
  user: User;

  articleFile: File;
  feeImage: File;

  result: String;
  errorMessage: string;
  messages: any;
  private subscription: Subscription = new Subscription();

  constructor(private articleService: ArticleService,
    private router: Router,
    private i18nService: InternacionalisationService) {
      this.messages = addArticleMessages[this.i18nService.language]
     }

  ngOnInit() {
    this.subscription.add(this.i18nService.languageChanged.subscribe(() => {
      this.messages = addArticleMessages[this.i18nService.language];
    }));
  }

  fileChange(files: FileList) {
    this.articleFile = files.item(0);
  }

  saveArticle() {
    if (this.checkFields()) {
      this.user = JSON.parse(sessionStorage.getItem('currentUser'));
      this.subscription.add(this.articleService.saveArticle(this.user.id).subscribe(article => {
        this.subscription.add(this.articleService.saveFile(this.articleFile, article.id).subscribe(() => {
          this.subscription.add(this.articleService.saveImage(this.feeImage, article.id)
            .subscribe(() => this.router.navigate(['/profile'])))
        }))
      }));
    }
  }

  checkFields() {
    if (this.feeImage === undefined || this.articleFile === undefined) {
      this.errorMessage = this.messages.errorMessages[0];
      return false;
    } else {
      return true;
    }
  }

  imageFileChange(files: any) {
    this.feeImage = files.item(0);
    this.parseImage();
  }

  parseImage() {
    const myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {
      this.article.feeImage = myReader.result as string;

    };
    myReader.readAsDataURL(this.feeImage);
  }
}
