import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../shared/auth.service';
import { Router } from '@angular/router';
import { InternacionalisationService } from '../shared/i18n.service';
import { loginMessages } from '../shared/i18n.messages';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  //remember: false;

  errorLogin = false;

  messages: any;

  private subscription: Subscription = new Subscription();

  constructor(private authService: AuthService,
    private router: Router,
    private i18nService: InternacionalisationService) {
    this.messages = loginMessages[this.i18nService.language];
  }

  ngOnInit() {
    this.subscription.add(this.i18nService.languageChanged.subscribe(() => {
      this.messages = loginMessages[this.i18nService.language];
    }));
  }

  authenticate() {
    this.subscription.add(
      this.authService.authenticate(this.username, this.password).subscribe(response => {
        this.router.navigate(['']);
        this.errorLogin = false;
      }, error => {
        this.errorLogin = true;
        this.authService.logout();
      })
    );
  }

}
