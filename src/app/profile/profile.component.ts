import { Component, OnInit } from '@angular/core';
import { User } from '../types/user.type';
import { Subscription } from 'rxjs';
import { ArticleService } from '../services/article.service';
import { Article, Review } from '../types/article.types';
import { ReviewService } from '../services/reviewService';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: User;
  //articles for user
  articles: Article[];
  articlesReviews: Map<string, Review[]> = new Map<string, Review[]>();
  reviewsFetched = 0;
  articleFile: File;
  modifiedArticles: boolean[] = [];
  bothAcceptedOrRejected: boolean[] = [];

  constructor(private articleService: ArticleService,
    private reviewService: ReviewService) { }
  private subscription: Subscription = new Subscription();

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.subscription.add(this.articleService.getArticles(this.user.id).subscribe(data => {
      this.articles = data;
      this.getArticlesReviews();
      this.isModified();
     // this.isBothAcceptedOrRejected();
    }));
  }

  isModified() {
    for (let i = 0; i < this.articles.length; i++) {
      if (((new Date(this.articles[i].lastModifiedDate).getTime() - new Date(this.articles[i].createdDate).getTime()) / 1000) < 10) {
        this.modifiedArticles[i] = false;
      } else {
        this.modifiedArticles[i] = true;
      }
    }
  }

  isBothAcceptedOrRejected() {
    for (let i = 0; i < this.articles.length; i++) {
      if((this.articlesReviews.get(this.articles[i].id)[0].accepted == true && 
      this.articlesReviews.get(this.articles[i].id)[1].accepted == true) || 
      (this.articlesReviews.get(this.articles[i].id)[0].accepted == false && 
      this.articlesReviews.get(this.articles[i].id)[1].accepted == false)) {
        this.bothAcceptedOrRejected[i] = true;
      } else {
        this.bothAcceptedOrRejected[i] = false;
      }
    }
  }

  getArticlesReviews() {
    for (let i = 0; i < this.articles.length; i++) {
      this.subscription.add(this.reviewService.getAllReviewsForArticle(this.articles[i]).subscribe(review => {
        this.articlesReviews.set(this.articles[i].id, review);
        this.reviewsFetched++;
      }))

    }
  }

  downloadReviewFirst(articleId: string) {
    this.subscription.add(this.reviewService.getReviewFiles(articleId, 1).subscribe(file => {
      var newBlob = new Blob([file], { type: "application/pdf" });
      // For other browsers: 
      // Create a link pointing to the ObjectURL containing the blob.
      const data = window.URL.createObjectURL(newBlob);

      var link = document.createElement('a');
      link.href = data;
      link.download = this.getArticleName(articleId) + "_review";

      // this is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

      setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    }));
  }

  getArticleName(articleId: string) {
    let articleName;
    this.articles.forEach(article => {
      if(article.id == articleId){
        articleName = article.title;
      }
    });
    return articleName;
  }

  downloadReviewSecond(articleId: string) {
    this.subscription.add(this.reviewService.getReviewFiles(articleId, 2).subscribe(file => {
      var newBlob = new Blob([file], { type: "application/pdf" });
      // For other browsers: 
      // Create a link pointing to the ObjectURL containing the blob.
      const data = window.URL.createObjectURL(newBlob);

      var link = document.createElement('a');
      link.href = data;
      link.download = this.getArticleName(articleId) + "_review";

      // this is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

      setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    }));
  }

  fileChange(files: FileList) {
    this.articleFile = files.item(0);
  }

  updateArticle(article: Article) {
    this.subscription.add(this.articleService.saveFile(this.articleFile, article.id).subscribe(() => {
    }));
    window.location.reload();
  }
  
  downloadArticle(articleId: string) {
    this.subscription.add(this.articleService.getArticleFile(articleId).subscribe(file => {
      var newBlob = new Blob([file], { type: "application/pdf" });
      // For other browsers: 
      // Create a link pointing to the ObjectURL containing the blob.
      const data = window.URL.createObjectURL(newBlob);

      var link = document.createElement('a');
      link.href = data;
      link.download = this.getArticleName(articleId);

      // this is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

      setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    }));
  }
  
}
