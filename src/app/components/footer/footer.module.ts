import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { FooterComponent } from './footer.component';

@NgModule({
  exports: [
    FooterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [
    FooterComponent
  ],
  providers: [
  ]
})
export class FooterModule { }
