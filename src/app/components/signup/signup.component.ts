import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import { UsersService } from 'src/app/services/users.service';
import { EmailService } from 'src/app/services/email.service';
import { InternacionalisationService } from 'src/app/shared/i18n.service';
import { signupMessages } from 'src/app/shared/i18n.messages';
import { User } from 'src/app/types/user.type';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  name: string;
  email: string;
  faculty: string;
  phone: string;
  password: string;
  repeatedPassword: string;
  existUser: User;

  errorMessage: string;
  messages: any;

  private subscription: Subscription = new Subscription();

  constructor(
    private router: Router,
    private authService: AuthService,
    private usersService: UsersService,
    private emailService: EmailService,
    private i18nService: InternacionalisationService) {
    this.messages = signupMessages[this.i18nService.language];
  }

  ngOnInit() {
    this.subscription.add(this.i18nService.languageChanged.subscribe(() => {
      this.messages = signupMessages[this.i18nService.language];
    }));
  }

  checkForm(form: any) {
    if (this.name === undefined || this.password === undefined || this.repeatedPassword === undefined || this.email === undefined || this.faculty == undefined) {
      this.errorMessage = this.messages.errorMessages[2];
      return false;
    }

    if (form.controls['email'].errors !== null) {
      this.errorMessage = this.messages.errorMessages[0];
      return false;
    }

    if (this.password !== this.repeatedPassword) {
      this.errorMessage = this.messages.errorMessages[1];
      return false;
    }

    //At least one upper case English letter, (?=.*?[A-Z])
    //At least one lower case English letter, (?=.*?[a-z])
    //At least one digit, (?=.*?[0-9])
    //At least one special character, (?=.*?[#?!@$%^&*-])
    //Minimum eight in length .{8,} (with the anchors)
    if (this.password.match("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$") === null) {
      this.errorMessage = this.messages.errorMessages[5];
      return false;
    }
    return true;
  }

  register(form: any) {
    if (this.checkForm(form)) {
      this.subscription.add(
        this.usersService.ifUserEmailExist(this.email).subscribe(user => {
          if (user == null) {
            this.subscription.add(this.usersService.createUser(this.email, this.password, this.name, this.faculty, 1, this.phone).subscribe(() => {
              this.subscription.add(
                this.emailService.sendSuccessfulSignupMail({ to: this.email, from: '' }).subscribe(() => { })
              );

              this.subscription.add(this.emailService.sendMail({
                from: 'contact.ebulletin@gmail.com',
                to: 'contact.ebulletin@gmail.com',
                subject: 'New Registration',
                content: 'email: ' + this.email + ' user: ' + this.name
              }).subscribe(() => { }));

              this.subscription.add(
                this.authService.authenticate(this.email, this.password).subscribe(userResponse => {
                  this.router.navigate(['/profile']);
                }, error => this.errorMessage = this.messages.errorMessages[4])
              );
            }))
          } else {
            this.errorMessage = this.messages.errorMessages[3];
          }
        })
      );
    }
  }
}
