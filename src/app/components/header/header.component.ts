import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { headerMessages } from 'src/app/shared/i18n.messages';
import { InternacionalisationService } from 'src/app/shared/i18n.service';
import { AuthService } from 'src/app/shared/auth.service';
import { Router } from '@angular/router';
import { Bulletin } from 'src/app/types/article.types';
import { BulletinService } from 'src/app/services/bulletin.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() styleClass: boolean;
  @Input() logo;

  menuItems: any[];
  logoHref: string;

  messages: any;
  user: any;
  login: boolean;

  bulletins: Bulletin[];

  private subscription: Subscription = new Subscription();

  constructor(private i18nService: InternacionalisationService,
    private authService: AuthService,
    private bulletinService: BulletinService,
    private router: Router) {
    this.logo = "assets/img/logo.png";
    this.messages = headerMessages[this.i18nService.language];
  }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.initialize();
    this.subscription.add(this.bulletinService.getBulletins().subscribe(data => {
      this.bulletins = data;
    }));
  }

  initialize() {
    this.logoHref = 'https://tu-sofia.bg/';
    if (this.authService.isUserLoggedIn()) {
      this.login = true;
      if (this.authService.getUser().role.id == 2) {
        this.menuItems = [
          {
            key: this.user.email,
            expandable: true,
            children: [
              {
                key: "View users",
                href: 'view-users',
                type: 'parent'
              },
              {
                key: "Manage articles",
                href: 'manage-articles',
                type: 'parent'
              },
              {
                key: this.messages.logout,
                href: 'logout',
                type: 'parent'
              }
            ]
          }
        ];
      }
      else if (this.authService.getUser().role.id == 1) {

        this.menuItems = [
          {
            key: this.user.email,
            expandable: true,
            children: [
              {
                key: this.messages.profile,
                href: 'profile',
                type: 'parent'
              },
              {
                key: this.messages.addArticle,
                href: 'add-article',
                type: 'parent'
              },
              {
                key: this.messages.logout,
                href: 'logout',
                type: 'parent'
              }
            ]
          }
        ];
      }
      else if (this.authService.getUser().role.id == 3) {
        this.menuItems = [
          {
            key: this.user.email,
            expandable: true,
            children: [
              {
                key: "View articles",
                href: 'view-articles',
                type: 'parent'
              },
              {
                key: this.messages.logout,
                href: 'logout',
                type: 'parent'
              }
            ]
          }
        ];
      }
    } else {
      this.login = false;
      this.menuItems = [
        {
          key: this.messages.signUp,
          expandable: false,
          href: 'signup'
        },
        {
          key: this.messages.login,
          expandable: false,
          href: 'login'
        }
      ];
    }
  }

  changeLanguage(language: string) {
    this.i18nService.emitLanguageChanged(language);
  }

}
