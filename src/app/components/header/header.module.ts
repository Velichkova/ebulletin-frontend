import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './header.component';

@NgModule({
  exports: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [
    HeaderComponent
  ],
  providers: [
  ]
})
export class HeaderModule { }
