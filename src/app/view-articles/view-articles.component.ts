import { Component, OnInit } from '@angular/core';
import { Article, Review } from '../types/article.types';
import { ArticleService } from '../services/article.service';
import { Subscription } from 'rxjs';
import { ReviewService } from '../services/reviewService';
import { User } from '../types/user.type';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-articles',
  templateUrl: './view-articles.component.html',
  styleUrls: ['./view-articles.component.css']
})
export class ViewArticlesComponent implements OnInit {

  articles: Article[];
  articleReviews: number[] = [];
  searchClicked: boolean;
  reviewFile: File;
  user: User;
  reviewsForCurrentUser: boolean[] = [];
  acceptedArticles: boolean[] = [];
  rejectedArticles: boolean[] = [];
  modifiedArticles: boolean[] = [];
  clicked = false;

  constructor(private articleService: ArticleService,
    private reviewService: ReviewService,
    private router: Router) { }
  private subscription: Subscription = new Subscription();

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.subscription.add(this.articleService.getAllArticles().subscribe(data => {
      this.articles = data;
      this.getArticlesEditorsNumber();
      this.isReviewed();
      this.isAccepted();
      this.isRejected();
      this.isModified();
    }))
  }

  isModified() {
    for (let i = 0; i < this.articles.length; i++) {
      if (((new Date(this.articles[i].lastModifiedDate).getTime() - new Date(this.articles[i].createdDate).getTime()) / 1000) < 10) {
        this.modifiedArticles[i] = false;
      } else {
        this.modifiedArticles[i] = true;
      }
    }
  }

  getArticlesEditorsNumber() {
    for (let i = 0; i < this.articles.length; i++) {
      this.subscription.add(this.reviewService.getAllReviewsForArticle(this.articles[i]).subscribe(data => {
        this.articleReviews[i] = data.length;
      }));
    }
    return this.articleReviews;
  }

  isReviewed() {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    let temp;
    for (let i = 0; i < this.articles.length; i++) {
      this.subscription.add(this.reviewService.getAllReviewsForArticle(this.articles[i]).subscribe(data => {
        if (data.length == 0) {
          this.reviewsForCurrentUser[i] = false;
        }
        //max 2
        //problem is when review are 2 
        if (data.some(review => review.user.id == this.user.id)) {
          this.reviewsForCurrentUser[i] = true;
        } else {
          this.reviewsForCurrentUser[i] = false;
        }
      }));
    }
  }


  fileChange(files: FileList) {
    this.reviewFile = files.item(0);
  }

  saveReview(articleId: string) {
    if (this.reviewFile != undefined) {
      this.user = JSON.parse(sessionStorage.getItem('currentUser'));
      this.subscription.add(this.reviewService.saveReview(this.user.id, articleId).subscribe(review => {
        this.subscription.add(this.reviewService.saveFile(this.reviewFile, review.id)
          .subscribe(() => this.router.navigate(['/view-articles'])));
      }));
      //window.location.reload();
    }
  }

  accept(article: Article) {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.subscription.add(this.reviewService.getAllReviewsForArticle(article).subscribe(reviews => {
      if (reviews.length == 0) {
        this.subscription.add(this.reviewService.saveReview(this.user.id, article.id).subscribe(review => {
          review.accepted = true;
          this.subscription.add(this.reviewService.updateReview(review).subscribe(data => {
            if(data != null){
            //window.location.reload();
            }
          }));
        }));
      } else if (reviews.length == 1) {
        reviews.forEach(review => {
          //if user have review file
          if (review.user.id == this.user.id && review.article.id == article.id) {
            review.accepted = true;
            this.subscription.add(this.reviewService.updateReview(review).subscribe(data => {
              if(data != null){
                //window.location.reload();
                }
            }));
          } else {
            // if user does not have review file
            this.subscription.add(this.reviewService.saveReview(this.user.id, article.id).subscribe(review => {
              review.accepted = true;
              this.subscription.add(this.reviewService.updateReview(review).subscribe(data => {
                if(data != null){
                  //window.location.reload();
                  }
              }));
            }));
          }
        });
      } else if (reviews.length == 2) {
        if (reviews.some(review => review.user.id == this.user.id && review.article.id == article.id)) {
          reviews.forEach(review => {
            //if user have review file
            if (review.user.id == this.user.id && review.article.id == article.id) {
              review.accepted = true;
              this.subscription.add(this.reviewService.updateReview(review).subscribe(data => {
                //window.location.reload();
              }));
            }
          });
        } else {
          //may not be executed
          // if user does not have review file
          this.subscription.add(this.reviewService.saveReview(this.user.id, article.id).subscribe(review => {
            review.accepted = true;
            this.subscription.add(this.reviewService.updateReview(review).subscribe(data => {
              if(data != null){
                //window.location.reload();
                }
            }));
          }));
        }
      }
    }));
  }

  isAccepted() {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    for (let i = 0; i < this.articles.length; i++) {
      this.subscription.add(this.reviewService.getAllReviewsForArticle(this.articles[i]).subscribe(data => {
        if (data.length == 0) {
          this.acceptedArticles[i] = false;
        }
        if (data.some(review => review.user.id == this.user.id && review.accepted == true)) {
          this.acceptedArticles[i] = true;
        } else {
          this.acceptedArticles[i] = false;
        }
      }));
    }
  }

  reject(article: Article) {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.subscription.add(this.reviewService.getAllReviewsForArticle(article).subscribe(reviews => {
      if (reviews.length == 0) {
        this.subscription.add(this.reviewService.saveReview(this.user.id, article.id).subscribe(review => {
          review.accepted = false;
          this.subscription.add(this.reviewService.updateReview(review).subscribe(data => {
            if(data != null){
              //window.location.reload();
              }
          }));
        }));
      } else if (reviews.length == 1) {
        reviews.forEach(review => {
          //if user have review file
          if (review.user.id == this.user.id && review.article.id == article.id) {
            review.accepted = false;
            this.subscription.add(this.reviewService.updateReview(review).subscribe(data => {
              if(data != null){
                //window.location.reload();
                }
            }));
          } else {
            // if user does not have review file
            this.subscription.add(this.reviewService.saveReview(this.user.id, article.id).subscribe(review => {
              review.accepted = false;
              this.subscription.add(this.reviewService.updateReview(review).subscribe(data => {
                if(data != null){
                  //window.location.reload();
                  }
              }));
            }));
          }
        });
      } else if (reviews.length == 2) {
        if (reviews.some(review => review.user.id == this.user.id && review.article.id == article.id)) {
          reviews.forEach(review => {
            //if user have review file
            if (review.user.id == this.user.id && review.article.id == article.id) {
              review.accepted = false;
              this.subscription.add(this.reviewService.updateReview(review).subscribe(data => {
                if(data != null){
                  //window.location.reload();
                  }
              }));
            }
          });
        } else {
          //may not be executed
          // if user does not have review file
          this.subscription.add(this.reviewService.saveReview(this.user.id, article.id).subscribe(review => {
            review.accepted = false;
            this.subscription.add(this.reviewService.updateReview(review).subscribe(data => {
              if(data != null){
                //window.location.reload();
                }
            }));
          }));
        }
      }
    }));
  }

  isRejected() {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    for (let i = 0; i < this.articles.length; i++) {
      this.subscription.add(this.reviewService.getAllReviewsForArticle(this.articles[i]).subscribe(data => {
        if (data.length == 0) {
          this.rejectedArticles[i] = false;
        }
        if (data.some(review => review.user.id == this.user.id && review.accepted == false)) {
          this.rejectedArticles[i] = true;
        } else {
          this.rejectedArticles[i] = false;
        }
      }));
    }
  }

  downloadArticle(articleId: string) {
    this.subscription.add(this.articleService.getArticleFile(articleId).subscribe(file => {
      var newBlob = new Blob([file], { type: "application/pdf" });
      // For other browsers: 
      // Create a link pointing to the ObjectURL containing the blob.
      const data = window.URL.createObjectURL(newBlob);

      var link = document.createElement('a');
      link.href = data;
      link.download = this.getArticleName(articleId);

      // this is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

      setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    }));
  }

  getArticleName(articleId: string) {
    let articleName;
    this.articles.forEach(article => {
      if (article.id == articleId) {
        articleName = article.title;
      }
    });
    return articleName;
  }

}
