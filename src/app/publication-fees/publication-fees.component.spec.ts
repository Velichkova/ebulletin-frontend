import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationFeesComponent } from './publication-fees.component';

describe('PublicationFeesComponent', () => {
  let component: PublicationFeesComponent;
  let fixture: ComponentFixture<PublicationFeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationFeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationFeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
