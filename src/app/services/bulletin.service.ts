import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AuthService } from '../shared/auth.service';
import { User } from '../types/user.type';
import { Observable } from 'rxjs';
import { Article, Bulletin } from '../types/article.types';

@Injectable()
export class BulletinService {

    constructor(private http: HttpClient,
        private authService: AuthService) { 
            this.authService.tryAuthenticate();
        }

    httpOptions = this.authService.getOptions();

    saveBulletin(startDate: Date, endDate: Date, issn: string) {
        const payload = {
            startDate: startDate,
            endDate: endDate,
            issn: issn
        }
        return this.http.post(this.authService.getBaseUrl() + 'bulletin/', payload, this.httpOptions);
    }

    getBulletins(){
        return this.http.get<Bulletin[]>(this.authService.getBaseUrl() + 'bulletin/', this.httpOptions);
    }
    
    getBulletinFile(bulletinId: string): Observable<Blob> {
        // this.http refers to HttpClient. Note here that you cannot use the generic get<Blob> as it does not compile: instead you "choose" the appropriate API in this way.
        return this.http.get(this.authService.getBaseUrl() + 'bulletin/' + bulletinId + '/file', { responseType: 'blob' });
    }

    getBulletin(bulletinId: string){
        
        return this.http.get<Bulletin>(this.authService.getBaseUrl() + 'bulletin/' + bulletinId, this.httpOptions);
    }
}