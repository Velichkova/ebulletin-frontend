import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../shared/auth.service';
import { Article, Review } from '../types/article.types';
import { Observable } from 'rxjs';

@Injectable()
export class ReviewService {
  delay(arg0: number) {
    throw new Error("Method not implemented.");
  }

  constructor(private http: HttpClient,
    private authService: AuthService) { 
      this.authService.tryAuthenticate();
    }

  httpOptions = this.authService.getOptions();

  createReview(accepted: boolean) {
    const payload = {
      accepted: accepted
    };

    let url = this.authService.getBaseUrl() + 'review';
    return this.http.post(url, payload, this.httpOptions);
  }

  updateReview(review: Review) {
    return this.http.put(this.authService.getBaseUrl() + 'review/' + review.id + '/accepted', review, this.httpOptions);
  }

  getAllReviewsForArticle(article: Article) {
    return this.http.get<Review[]>(this.authService.getBaseUrl() + 'review/' + article.id, this.httpOptions);
  }

  saveReview(userId: string, articleId: string): Observable<Review> {
    const payload = {
      user: {
        id: userId
      },
      article: {
        id: articleId
      }
    }
    return this.http.post<Review>(this.authService.getBaseUrl() + 'review/', payload, this.httpOptions);
  }

  saveFile(reviewFile: File, reviewId: any) {
    const body = new FormData();
    body.append('file', reviewFile, reviewFile.name);
    const httpOptions = {
      headers: new HttpHeaders({
        resourceType: 'blob'
      })
    };
    return this.http.post(this.authService.getBaseUrl() + 'review/' + reviewId + '/file', body, httpOptions);
  }

  getReviewFiles(articleId: string, reviewNeeded: number){
    // this.http refers to HttpClient. Note here that you cannot use the generic get<Blob> as it does not compile: instead you "choose" the appropriate API in this way.
    return this.http.get(this.authService.getBaseUrl() + 'review/' + articleId + '/' + reviewNeeded, { responseType: 'blob' });
  }

}