import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AuthService } from '../shared/auth.service';
import { User } from '../types/user.type';
import { Observable } from 'rxjs';
import { Article, Bulletin } from '../types/article.types';

@Injectable()
export class ArticleService {

    constructor(private http: HttpClient,
        private authService: AuthService) { 
            this.authService.tryAuthenticate();
        }

    httpOptions = this.authService.getOptions();

    saveArticle(userId: string): Observable<Article> {
        const payload = {
            user: {
                id: userId
            }
        }
        return this.http.post<Article>(this.authService.getBaseUrl() + 'article/', payload, this.httpOptions);
    }

    saveFile(articleFile: File, articleId: any) {
        const body = new FormData();
        body.append('file', articleFile, articleFile.name);
        
        const httpOptions = {
            headers: new HttpHeaders({
                resourceType: 'blob'
            })
        };
        return this.http.post(this.authService.getBaseUrl() + 'article/' + articleId + '/file', body, httpOptions);
    }

    saveImage(feeImage: File, articleId: any) {
        const body = new FormData();
        body.append('file', feeImage, feeImage.name);
        
        const httpOptions = {
            headers: new HttpHeaders({
                resourceType: 'blob'
            })
        };
        return this.http.post(this.authService.getBaseUrl() + 'article/' + articleId + '/feeImage', body, httpOptions);
    }


    getArticles(userId: string) {
        return this.http.get<Article[]>(this.authService.getBaseUrl() + 'article/' + userId, this.httpOptions);
    }

    getAllArticles() {
        return this.http.get<Article[]>(this.authService.getBaseUrl() + 'article/', this.httpOptions);
    }

    getArticleFile(articleId: string): Observable<Blob> {
        // this.http refers to HttpClient. Note here that you cannot use the generic get<Blob> as it does not compile: instead you "choose" the appropriate API in this way.
        return this.http.get(this.authService.getBaseUrl() + 'article/' + articleId + '/file', { responseType: 'blob' });
    }

    getArticleFeeImageFile(articleId: string): Observable<Blob> {
        // this.http refers to HttpClient. Note here that you cannot use the generic get<Blob> as it does not compile: instead you "choose" the appropriate API in this way.
        return this.http.get(this.authService.getBaseUrl() + 'article/' + articleId + '/feeImage', { responseType: 'blob' });
    }

    addBulletin(article: Article) {
        return this.http.put(this.authService.getBaseUrl() + 'article/' + article.id, article, this.httpOptions);
    }

    //for all bulletins-home page
    getAllArchiveArticles() {
        return this.http.get<Article[]>(this.authService.getBaseUrl() + 'articleArchive/', this.httpOptions);
    }
}