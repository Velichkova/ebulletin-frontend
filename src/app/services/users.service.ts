import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../shared/auth.service';
import { User } from '../types/user.type';
import { Observable } from 'rxjs';

@Injectable()
export class UsersService {

  constructor(private http: HttpClient,
    private authService: AuthService) { 
      this.authService.tryAuthenticate();
    }

    httpOptions = this.authService.getOptions();

    createUser(username: string, password: string, name: string, faculty: string, role = 1, phone: string) {
      const payload = {
        username: username,
        password: password,
        email: username,
        name: name,
        faculty: faculty,
        phone: phone,
        role: {
          id: role,
          name: "user",
          description: "normal user"
        }
      };
      
      let url = this.authService.getBaseUrl() + 'users';
        return this.http.post(url, payload, this.httpOptions);
    }

    getUserWithForgottenPasswordToken(token: string){
      return this.http.get<User>(this.authService.getBaseUrl() + 'users/forgotten-password?token=' + token, this.httpOptions);
    }

    updateUser(user: User) {
      return this.http.put<User>(this.authService.getBaseUrl() + 'users/' + user.id, user, this.httpOptions);
    }
    
    resetPassword(email: string) {
      return this.http.put(this.authService.getBaseUrl() + 'users/forgotten-password?email=' + email, this.httpOptions);
    }

    getUsers(){
      return this.http.get<User[]>(this.authService.getBaseUrl() + 'users', this.httpOptions);
    }

    ifUserEmailExist(email: string): Observable<User> {
      return this.http.get<User>(this.authService.getBaseUrl() + 'users/' + email, this.httpOptions);
    }
}
