import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { AuthService } from '../shared/auth.service';
import { Email } from '../types/email.type';

@Injectable()
export class EmailService {

  constructor(private http: HttpClient,
    private authService: AuthService) { 
      this.authService.tryAuthenticate();
    }

  httpOptions = this.authService.getOptions();

  sendMail(email: Email) {
    return this.http.post(this.authService.getBaseUrl() + 'email', email, this.httpOptions);
  }
  sendSuccessfulSignupMail(email: Email) {
    return this.http.post(this.authService.getBaseUrl() + 'email/signup', email, this.httpOptions);
  }
}
