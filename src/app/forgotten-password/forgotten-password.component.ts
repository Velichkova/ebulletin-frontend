import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UsersService } from '../services/users.service';
import { User } from '../types/user.type';
import { InternacionalisationService } from '../shared/i18n.service';
import { forgottenPasswordMessages } from '../shared/i18n.messages';
import { AuthService } from '../shared/auth.service';
import { UtilsService } from '../shared/utils.service';

@Component({
  selector: 'app-forgotten-password',
  templateUrl: './forgotten-password.component.html',
  styleUrls: ['./forgotten-password.component.css']
})
export class ForgottenPasswordComponent implements OnInit {

  user: User;
  emailEntryView: Boolean = false;
  newPasswordsMissmatch: Boolean = false;
  invalidEmailError: Boolean = false;
  emailSent: Boolean = false;
  emailSendError: Boolean = false;
  email: string;
  newPassword: string;
  newPasswordAgain: string;

  messages: any;
  
  private subscription: Subscription = new Subscription();
  
  constructor(private router: Router,
    private usersService: UsersService,
    private route: ActivatedRoute,
    private i18nService: InternacionalisationService,
    private authService: AuthService,
    private utilsService: UtilsService) { 
      this.messages = forgottenPasswordMessages[this.i18nService.language];
    }

  ngOnInit() {
    const token = this.route.snapshot.paramMap.get('token');

    if (token !== null) {
      this.subscription.add(this.usersService.getUserWithForgottenPasswordToken(token).subscribe(response => {
        this.user = response;
      }, () => {
        this.router.navigate(['/']);
      }));
    } else {
      this.emailEntryView = true;
    }

    this.subscription.add(this.i18nService.languageChanged.subscribe(() => {
      this.messages = forgottenPasswordMessages[this.i18nService.language];
    }));
  }

  sendResetEmail(form: any) {
    this.invalidEmailError = false;
    this.emailSendError = false;

    if (form.controls['email'].errors !== null) {
      this.invalidEmailError = true;
    } else {
      this.subscription.add(this.usersService.resetPassword(this.email).subscribe(() => {
        this.emailSent = true;
        this.email = undefined;
        this.utilsService.delay(2500).then(() => this.emailSent = false);
      }, () => {
        this.emailSendError = true;
      }));
    }
  }

  resetPassword() {
    this.newPasswordsMissmatch = false;
    if (this.newPassword !== this.newPasswordAgain) {
      this.newPasswordsMissmatch = true;
    } else {
      this.user.password = this.newPassword;
      this.user.forgottenPasswordToken = '';
      this.subscription.add(this.usersService.updateUser(this.user).subscribe(() => {
        this.subscription.add(this.authService.authenticate(this.user.username, this.user.password).subscribe(auth => {
          //this.authService.setUser(auth); cookie
          this.router.navigate(['/']);
        }));
      }));
    }
  }

}
