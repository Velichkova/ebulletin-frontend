import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { FooterModule } from './components/footer/footer.module';
import { HeaderModule } from './components/header/header.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SignupComponent } from './components/signup/signup.component';

import { httpInterceptorProviders } from './shared/interceptors.barrel';
import { UsersService } from './services/users.service';
import { AuthService } from './shared/auth.service';
import { EmailService } from './services/email.service';
import { LoginComponent } from './login/login.component';
import { ForgottenPasswordComponent } from './forgotten-password/forgotten-password.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGaurdService } from './shared/authGaurdService';
import { AddArticleComponent } from './add-article/add-article.component';
import { ArticleService } from './services/article.service';
import { ProfileComponent } from './profile/profile.component';
import { ViewUsersComponent } from './view-users/view-users.component';
import { ViewArticlesComponent } from './view-articles/view-articles.component';
import { ReviewService } from './services/reviewService';
import { AboutComponent } from './about/about.component';
import { FormattingGuidelinesComponent } from './formatting-guidelines/formatting-guidelines.component';
import { ReviewPolicyComponent } from './review-policy/review-policy.component';
import { PublicationFeesComponent } from './publication-fees/publication-fees.component';
import { PublicationEthicsComponent } from './publication-ethics/publication-ethics.component';
import { EditorsComponent } from './editors/editors.component';
import { ManageArticlesComponent } from './manage-articles/manage-articles.component';
import { BulletinService } from './services/bulletin.service';
import { IssueComponent } from './issue/issue.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    ForgottenPasswordComponent,
    LogoutComponent,
    AddArticleComponent,
    ProfileComponent,
    ViewUsersComponent,
    ViewArticlesComponent,
    AboutComponent,
    FormattingGuidelinesComponent,
    ReviewPolicyComponent,
    PublicationFeesComponent,
    PublicationEthicsComponent,
    EditorsComponent,
    ManageArticlesComponent,
    IssueComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FooterModule,
    HeaderModule,
    SharedModule.forRoot(),
    HttpClientModule,
    FormsModule
  ],
  providers: [
    AuthService,
    UsersService,
    EmailService,
    AuthGaurdService,
    ArticleService,
    ReviewService,
    BulletinService,
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
