import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './components/signup/signup.component';
import { LoginComponent } from './login/login.component';
import { ForgottenPasswordComponent } from './forgotten-password/forgotten-password.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGaurdService } from './shared/authGaurdService';
import { AddArticleComponent } from './add-article/add-article.component';
import { ProfileComponent } from './profile/profile.component';
import { ViewUsersComponent } from './view-users/view-users.component';
import { ViewArticlesComponent } from './view-articles/view-articles.component';
import { AboutComponent } from './about/about.component';
import { FormattingGuidelinesComponent } from './formatting-guidelines/formatting-guidelines.component';
import { ReviewPolicyComponent } from './review-policy/review-policy.component';
import { PublicationFeesComponent } from './publication-fees/publication-fees.component';
import { PublicationEthicsComponent } from './publication-ethics/publication-ethics.component';
import { EditorsComponent } from './editors/editors.component';
import { ManageArticlesComponent } from './manage-articles/manage-articles.component';
import { IssueComponent } from './issue/issue.component';
import { HeaderComponent } from './components/header/header.component';

const routes: Routes = [
  { path: 'signup', component: SignupComponent },
  {path: 'login', component: LoginComponent},
  {path: '', component: AboutComponent},
  {path: 'issue/:id', component: IssueComponent},
  {path: 'forgotten-password', component: ForgottenPasswordComponent},
  {path: 'forgotten-password/:token', component: ForgottenPasswordComponent},
  {path: 'logout', component: LogoutComponent, canActivate:[AuthGaurdService]},
  {path: 'add-article', component: AddArticleComponent, canActivate:[AuthGaurdService]},
  {path: 'profile', component: ProfileComponent, canActivate:[AuthGaurdService]},
  {path: 'view-users', component: ViewUsersComponent, canActivate:[AuthGaurdService]},
  {path: 'view-articles', component: ViewArticlesComponent, canActivate:[AuthGaurdService]},
  {path: 'about', component: AboutComponent},
  {path: 'formatting-guidelines', component:FormattingGuidelinesComponent},
  {path: 'review-policy', component:ReviewPolicyComponent},
  {path: 'publication-fees', component:PublicationFeesComponent},
  {path: 'publication-ethics', component:PublicationEthicsComponent},
  {path: 'editors', component:EditorsComponent},
  {path: 'manage-articles', component:ManageArticlesComponent, canActivate:[AuthGaurdService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
