import { Component, OnInit } from '@angular/core';
import { Article, Bulletin } from '../types/article.types';
import { ArticleService } from '../services/article.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { BulletinService } from '../services/bulletin.service';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.css']
})
export class IssueComponent implements OnInit {

  articles: Article[] = [];
  bulletin: Bulletin;
  vol: string;
  month: string;

  public hideRuleContent: boolean[] = [];
  public buttonName: any = 'Expand';

  constructor(private articleService: ArticleService,
    private route: ActivatedRoute,
    private bulletinService: BulletinService,
    private authService: AuthService) { }
  private subscription: Subscription = new Subscription();

  ngOnInit() {
    const bulletinId = this.route.snapshot.paramMap.get('id');
    this.subscription.add(this.articleService.getAllArchiveArticles().subscribe(data => {
      data.forEach(article => {
        if (article.bulletin.id == bulletinId) {
          this.articles.push(article);
        }
      });
    }));

    this.subscription.add(this.bulletinService.getBulletin(bulletinId).subscribe(data => {
      this.bulletin = data;
    }));

    this.authService.tryAuthenticate();
  }


  toggle(index) {
    // toggle based on index
    this.hideRuleContent[index] = !this.hideRuleContent[index];
  }
}
