import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewPolicyComponent } from './review-policy.component';

describe('ReviewPolicyComponent', () => {
  let component: ReviewPolicyComponent;
  let fixture: ComponentFixture<ReviewPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
