import { Component, OnInit } from '@angular/core';
import { Article, Review, Bulletin } from '../types/article.types';
import { Subscription } from 'rxjs';
import { ArticleService } from '../services/article.service';
import { ReviewService } from '../services/reviewService';
import { BulletinService } from '../services/bulletin.service';

@Component({
  selector: 'app-manage-articles',
  templateUrl: './manage-articles.component.html',
  styleUrls: ['./manage-articles.component.css']
})
export class ManageArticlesComponent implements OnInit {

  articles: Article[];
  articlesReviews: Map<string, Review[]> = new Map<string, Review[]>();
  reviewsFetched = 0;
  curDate = new Date();
  bulletins: Bulletin[];
  curBulletin: Bulletin;
  curFrom: Date;
  curTo: Date;
  startDate: Date;
  endDate: Date;
  issn: string;
  addedArticles: boolean[] = [];

  constructor(private articleService: ArticleService,
    private reviewService: ReviewService,
    private bulletinService: BulletinService) {
  }
  private subscription: Subscription = new Subscription();

  ngOnInit() {
    this.subscription.add(this.articleService.getAllArticles().subscribe(data => {
      this.articles = data;
      this.getArticlesReviews();
      this.getBulletins();
      this.isAdded();
    }));
  }

  getArticlesReviews() {
    for (let i = 0; i < this.articles.length; i++) {
      this.subscription.add(this.reviewService.getAllReviewsForArticle(this.articles[i]).subscribe(review => {
        this.articlesReviews.set(this.articles[i].id, review);
        this.reviewsFetched++;
      }))
    }
  }

  getBulletins() {
    this.subscription.add(this.bulletinService.getBulletins().subscribe(data => {
      this.bulletins = data;
      this.checkIfBulletinForCurDate();
    }));
  }

  createBulletin() {
    this.subscription.add(this.bulletinService.saveBulletin(this.startDate, this.endDate, this.issn).subscribe(() => {
    }));
    window.location.reload();
  }

  checkIfBulletinForCurDate() {
    if (this.bulletins.length != 0) {
      this.bulletins.forEach(bulletin => {
        if ((new Date(this.curDate)) >= (new Date(bulletin.startDate)) &&
          (new Date(this.curDate)) <= (new Date(bulletin.endDate))) {
          this.curBulletin = bulletin;
          this.curFrom = bulletin.startDate;
          this.curTo = bulletin.endDate;
        }
      });
    }
  }

  addToCurrentBulletin(article: Article) {
    article.bulletin = this.curBulletin;
    this.subscription.add(this.articleService.addBulletin(article).subscribe(() => {
    }));
    window.location.reload();
  }

  downloadArticle(articleId: string) {
    this.subscription.add(this.articleService.getArticleFile(articleId).subscribe(file => {
      var newBlob = new Blob([file], { type: "application/pdf" });
      // For other browsers: 
      // Create a link pointing to the ObjectURL containing the blob.
      const data = window.URL.createObjectURL(newBlob);

      var link = document.createElement('a');
      link.href = data;
      link.download = this.getArticleName(articleId);

      // this is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

      setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    }));
  }

  downloadFeeImage(articleId: string) {
    this.subscription.add(this.articleService.getArticleFeeImageFile(articleId).subscribe(file => {
      var newBlob = new Blob([file], { type: "image/jpg" });
      // For other browsers: 
      // Create a link pointing to the ObjectURL containing the blob.
      const data = window.URL.createObjectURL(newBlob);

      var link = document.createElement('a');
      link.href = data;
      link.download = this.getArticleName(articleId);

      // this is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

      setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    }));
  }

  getArticleName(articleId: string) {
    let articleName;
    this.articles.forEach(article => {
      if (article.id == articleId) {
        articleName = article.title;
      }
    });
    return articleName;
  }

  isAdded() {
    for (let i = 0; i < this.articles.length; i++) {
      if(this.articles[i].bulletin == null) {
        this.addedArticles[i] = false;
      } else {
        this.addedArticles[i] = true;
      }
    }
  }

  generateBulletin() {
    this.subscription.add(this.bulletinService.getBulletinFile(this.curBulletin.id).subscribe(file => {
      var newBlob = new Blob([file], { type: "application/pdf" });
      // For other browsers: 
      // Create a link pointing to the ObjectURL containing the blob.
      const data = window.URL.createObjectURL(newBlob);

      var link = document.createElement('a');
      link.href = data;
      link.download = this.curBulletin.id;

      // this is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

      setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    }));
  }
}
