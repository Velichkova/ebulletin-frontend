import { Injectable } from '@angular/core';

@Injectable()
export class AuthConsts {
    basicAuthString: string;
    
    getBaseUrl() {
        return 'http://localhost:8080/v1/';
    }
}
