import { Injectable } from '@angular/core';
import { User } from '../types/user.type';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AuthConsts } from './auth.consts';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: User;
  authString: string;

  constructor(private http: HttpClient,
    private consts: AuthConsts) { }

    tryAuthenticate() {
      const storage = sessionStorage.getItem('auth');
      if (storage) {
        this.consts.basicAuthString = storage;
        const items = atob(this.consts.basicAuthString).split(':');
        const userMetadata = sessionStorage.getItem('user');
        if (userMetadata) {
          this.user = JSON.parse(userMetadata);
        }
        this.authenticate(items[0], items[1]).subscribe(response => this.setUser(response), error => console.warn('could not authenticate'));
        return true;
      }
  
      if (this.user) {
        return true;
      }
  
      return false;
    }

  authenticate(username: string, password: string) {
    this.authString = btoa(username + ':' + password)
    sessionStorage.setItem('auth', this.authString);
    this.consts.basicAuthString = this.authString;

    return this.http.post<User>(this.getBaseUrl() + 'users/authenticate', 
    { username: username, password: password }, this.getOptions())
    .pipe( 
      map(user => {
        sessionStorage.setItem('currentUser', JSON.stringify(user));
        return user;
      })
    );
  }

  setUser(user: User) {
    this.user = user;
      sessionStorage.setItem('user', JSON.stringify(this.user));
      sessionStorage.setItem('userRole', String(this.user.role.id));
  }
  getBasicAuthString(): string {
    return this.consts.basicAuthString;
  }

  getUser(){
    return JSON.parse(sessionStorage.getItem('currentUser'));
  }
  
  isUserLoggedIn(): boolean {
    let user = sessionStorage.getItem('currentUser')
    return !(user === null)
  }

  logOut() {
    sessionStorage.removeItem('currentUser')
  }
  
  getBaseUrl(): string {
    return this.consts.getBaseUrl();
  }

  getOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
  }

  logout() {
    this.user = undefined;
  }
}
