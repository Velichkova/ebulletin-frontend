import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable()
export class UtilsService {

  constructor(private authService: AuthService) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': this.authService.getBasicAuthString()
    })
  };

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
