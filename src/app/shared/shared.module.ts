import { NgModule, ModuleWithProviders, Optional, SkipSelf } from '@angular/core';
import { InternacionalisationService } from './i18n.service';
import { AuthService } from './auth.service';
import { AuthConsts } from './auth.consts';
import { UtilsService } from './utils.service';

@NgModule({
  imports: [],
  declarations: []
})
export class SharedModule {
  constructor (@Optional() @SkipSelf() parentModule: SharedModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [AuthService, UtilsService, AuthConsts, InternacionalisationService]
    };
  }
}
