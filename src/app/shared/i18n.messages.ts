export const signupMessages = {
    en: {
      signUp: 'Sign up',
      emailPlaceholder: 'E-mail*',
      passwordPlaceholder: 'Password*',
      namePlaceholder: 'Name*',
      facultyPlaceholder: 'Faculty*',
      phonePlaceholder: 'Phone',
      passwordAgainPlaceholder: 'Password again*',
      errorMessages: [
        'The e-mail address is invalid',
        'Passwords does not match',
        'Fill all required fields',
        'User with that email exists',
        'Something went wrong',
        'Password length must be minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character!'
      ]
    },
    bg: {
      signUp: 'Регистрация',
      emailPlaceholder: 'Имейл*',
      name: 'Име*',
      faculty: 'Факултет*',
      phonePlaceholder: 'Телефон',
      passwordPlaceholder: 'Парола*',
      passwordAgainPlceholder: 'Потвърждение на паролата*',
      errorMessages: [
        'Имейл адресът е невалиден',
        'Паролите не съвпадат',
        'Попълнете всички задължителни полета',
        'Потребител с това име същестува',
        'Нещо се обърка',
        'Дължината на паролата трябва да е между 8 символа, поне 1 голяма буква, 1 малка и 1 число, 1 специален символ!'
      ]
    }
  };

  export const headerMessages = {
    en: {
      addArticle: 'Add article',
      profile: 'Profile',
      logout: 'Log out',
      signUp: 'Sign up',
      login: 'Log in'
    },
    bg: {
      addArticle: 'Добави статия',
      profile: 'Профил',
      logout: 'Отписване',
      signUp: 'Регистрация',
      login: 'Вписване'
    }
  };

  export const loginMessages = {
    en: {
      login: 'Log in',
      emailPlaceholder: 'E-mail',
      passwordPlaceholder: 'Password',
      forgottenPassword: 'Forgotten password?',
      wrongCredentials: 'Wrong username or password. Check your credentials and try again',
      notAMember: 'Not a Member?',
      signUp: 'Sign up'
    },
    bg: {
      login: 'Вписване',
      emailPlaceholder: 'Имейл',
      passwordPlaceholder: 'Парола',
      forgottenPassword: 'Забравена парола?',
      wrongCredentials: 'Грешен имейл или парола',
      notAMember: 'Не сте член?',
      signUp: 'Регистрация'
    }
  };

  export const forgottenPasswordMessages = {
    en: {
      header: 'Recover password',
      inputPlaceholder: 'Enter your e-mail',
      invalidEmail: 'This e-mail is invalid',
      success: 'E-mail with instructions was sent',
      recover: 'Recover',
      remembered: 'Remembered?',
      signIn: 'Sign in',
      header2: 'Choose a new password',
      newPasswordPlaceholder: 'Enter your new password',
      newPasswordAgainPlaceholder: 'Re-enter your new password',
      passwordMissmatch: 'Passwords does not match',
      change: 'Change',
      somethingWrong: 'Something went wrong. Try again'
    },
    bg: {
      header: 'Възстановяване на парола',
      inputPlaceholder: 'Въведете имейл',
      invalidEmail: 'Невалиден имейл',
      success: 'Съобщение с инструкции беше изпратено на посочения мейл',
      recover: 'Възстанови',
      remembered: 'Спомнихте си',
      signIn: 'Вписване',
      header2: 'Изберете нова парола',
      newPasswordPlaceholder: 'Въведете новата парола',
      newPasswordAgainPlaceholder: 'Новата парола отново',
      passwordMissmatch: 'Паролите не съвпадат',
      change: 'Смени',
      somethingWrong: 'Нещо се обърка. Опитайте отново'
    }
  };

  export const addArticleMessages = {
    en: {
      errorMessages: [
        'Both files are mandatory!'
      ]
    },
    bg: {
      errorMessages: [
        'И двата файла са задължителни!'
      ]
    }
  };