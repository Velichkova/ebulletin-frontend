import { Injectable, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class InternacionalisationService implements OnInit {

  language: string;
  public languageChanged = new Subject();

  constructor() {
    this.language = sessionStorage.getItem('language') || 'en';
  }

  ngOnInit() {
    this.language = sessionStorage.getItem('language') || 'en';
  }

  emitLanguageChanged(language: string) {
    this.language = language;
    sessionStorage.setItem('language', language);
    this.languageChanged.next();
    window.location.reload();
  }
}
