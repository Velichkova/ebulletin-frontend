import { Injectable } from '@angular/core';
import { AuthConsts } from './auth.consts';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private auth: AuthConsts) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // Get the auth token from the service.
    let authToken: string;
    if (this.auth.basicAuthString) {
      authToken = 'Basic ' + this.auth.basicAuthString;
    } else {
      authToken = 'Basic dXNlckNyZWF0b3I6cGFzc3dvcmQ=';
    }

    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    const authReq = req.clone({
      headers: req.headers.set('Authorization', authToken)
    });

    // send cloned request with header to the next handler.
    return next.handle(authReq);
  }
}
